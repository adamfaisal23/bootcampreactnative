//Tugas If-Else

var name 	= "John"
var peran	= ""
if (name== "" && peran=="") {
	console.log("Nama harus di isi!")
} else if (name=="John" && peran=="") {
	console.log("Halo John, pilih peranmu untuk memulai game")
}
if (name=="Jane" && peran=="Penyihir") {
	console.log("Selamat datang di Dunia Werewolf, Jane")
	console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
}
if (name=="Jenita" && peran=="Guard") {
	console.log("Selamat datang di Dunia Werewolf, Jenita")
	console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
}
if (name=="Junaedi" && peran=="Warewolf") {
	console.log("Selamat datang di Dunia Werewolf, Junaedi")
	console.log("Halo Warewolf Junaedi,  Kamu akan memakan mangsa setiap malam!")
}
console.log("")


//Tugas Switch Case
var tanggal = 21;
var bulan 	= 1;
var tahun	= 1945;

if(tanggal >= 1 && tanggal <= 31 && bulan >= 1 && bulan <= 12 && tahun >= 1900 && tahun <=2200) {

switch(bulan)
	{
		case 1:{console.log(tanggal+' Januari '+tahun); break;}
		case 2:{console.log(tanggal+' Februari '+tahun); break;}
		case 3:{console.log(tanggal+' Maret '+tahun); break;}
		case 4:{console.log(tanggal+' April '+tahun); break;}
		case 5:{console.log(tanggal+' Mei '+tahun); break;}
		case 6:{console.log(tanggal+' Juni '+tahun); break;}
		case 7:{console.log(tanggal+' Juli '+tahun); break;}
		case 8:{console.log(tanggal+' Agustus '+tahun); break;}
		case 9:{console.log(tanggal+' September '+tahun); break;}
		case 10:{console.log(tanggal+' Oktober '+tahun); break;}
		case 11:{console.log(tanggal+' November '+tahun); break;}
		case 12:{console.log(tanggal+' Desember '+tahun); break;}
	}
}
